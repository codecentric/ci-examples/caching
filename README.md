# caching

Der GitLab-CI Runner Cache kann dazu verwendet werden build-dependencies über mehrere Pipeline Aufrufe hinweg bereit zu stellen. Für Maven builds kann so zum Beispiel das `.m2`-Verzeichnis gecached werden, um die bereits beim vorigen Build heruntergeladenen Abhängigkeiten zu auch beim nächsten Buildzur Verfügung zu haben.

Vorteile:
- deutlich kürzere Pipeline-Durchläufe!
- weniger Traffic für die CI-Infrastruktur!

## Achtung!

- Caches im Allgemeinen können mit einem `hit` oder einem `miss` antworten. Dieses Prinzip gilt auch hier. Die Pipeline sollte nicht deshalb fehlschlagen, weil der Cache leer ist! Wenn der Cache leer ist, sollte er durch die Pipeline neu befüllt werden.

- Es sollte davon abgesehen werden Cache-Keys zu verwenden die den Cache auf den aktuellen Pipelinedurchlauf beschränken. Dies führt dazu, dass sich in dem Cache Verzeichnisse mit Daten anhäufen auf die nie wieder zugegriffen wird. (Je nach Datenmenge kann dies schnell zu hohen Kosten führen!) Beispiel: i

```
##### DON´T DO! #####
myjob:
  script: test
  cache:
    key: $CI_COMMIT_SHA
    paths:
      - binaries/
##### DON´T DO! #####
```

## Cache löschen

Sollte es zu Problemen mit dem Cache kommen, oder sollten sich dort Daten angehäuft haben die nicht mehr benötigt werden, dann kann der Cache gelöscht werden. Dafür gibt es in dem GitLab Projekt unter `CI / CD` -> `Pipelines` einen Knopf `Clear Runner Caches` (oben rechts).

